# get me some SW for Arch Linux with pacman:

# maybe some stuff already installed (see vim , nano, neofetch)
sudo pacman -S xf86-video-fbdev xorg xorg-xinit xterm lxappearance \
	nitrogen picom awesome-terminal-fonts xdotool xclip gsimplecal arandr \
	xmonad xmonad-contrib xmobar dmenu dzen2 rofi trayer volumeicon \
	rxvt-unicode urxvt-perls firefox mc neovim neofetch xed \
	vim nano ranger pcmanfm w3m imlib2 highlight poppler sxiv zsh \
	cowsay lolcat sl cmatrix fortune-mod figlet asciiquarium banner \
	nemo nemo-fileroller nemo-image-converter nemo-preview nemo-terminal

# and maybe you like also:
sudo pacman -S alacritty qutebrowser pulseaudio pulseaudio-alsa alsa-utils

# aproximately 460 Packages:
# Gesamtgröße des Downloads:            396,75
 MiB
# Gesamtgröße der installierten Pakete:  1796,96 MiB
# Größendifferenz der Aktualisierung:  1790,62 MiB



# look at nemo stuff: nemo nemo-fileroller nemo-image-converter nemo-preview nemo-terminal
# Gesamtgröße des Downloads:           176,03 MiB
# Gesamtgröße der installierten Pakete:  770,99 MiB
# compare to pcmanfm:
# Gesamtgröße des Downloads:            21,11 MiB
# Gesamtgröße der installierten Pakete:  107,04 MiB
# compare to thunar:
# Gesamtgröße des Downloads:            98,72 MiB
# Gesamtgröße der installierten Pakete:  465,87 MiB



# check: w3imgdisplay works?
# which w3imgdisplay or w3m xkcd.com (in xterm)
# maybe: sudo ln -s /usr/lib/w3m/w3mimgdisplay /usr/bin/

# xinit for Xmonad Start
# end of config delete stuff for twm and do the xmonad exec...
cp /etc/X11/xinit/xinitrc ~/.xinitrc
vim .xinitrc

	# ~.xinitrc
	#
	# nitrogen --restore &
	picom  --config ~/.config/picom/picom.conf &
	exec xmonad


# to JoeB: use your git stuff to start with!

# Fonts:
# MesloLGS NF
# copy manually in /usr/share/fonts/TTF/
# or for user in /home/joeb/.fonts
# followed by: fc-cache -fc
# FontAwesome (awesome-terminal-fonts)
# Checking with: fc-list | grep ...

# Do not forget all the stuff needed:
# ~/.Xressources and xrdb -merge ~/.Xressources
# ~.config/xmobar/xmobarrc
# ~/.xmonad/xmonad.hs and ~/.xmonad folder stuff
# copy stuff from your stuff-folder .config: gsimplecal, ranger, rofi
# xmonad --recompile

# picom config - first copy and the maybe get vsync to false (line 212ff)
mkdir .config/picom
cp /etc/xdg/picom.conf ~/.config/picom/  # or use your git-repo stuff
# alternative: AUR 
yay -S picom-jonaburg-git                # with special picom.conf!


# Arch Linux as Vbox Guest - obviously not for Hyper-V
# https://wiki.archlinux.org/index.php/VirtualBox/Install_Arch_Linux_as_a_guest
# remark: for Standard-Kernel linux and Kernel >= 5.6 no DKSM like virtualbox-guest-dkms
pacman -S virtualbox-guest-utils
# after reboot check Status of vboxservice.service
systemctl status vboxservice.service
# and then you maybe need to do
sudo systemctl start vboxservice.service
sudo systemctl enable vboxservice.service
# should look like this after reboots:
	#lsmod | grep ^vbox
	vboxsf                 40960  0
	vboxvideo              28672  0
	vboxguest              49152  3 vboxsf
# delete/comment lines in xmonad.hs for screenlayout scripts


# German Keyboard - we need in console:
sudo localectl set-x11-keymap de pc105 nodeadkeys compose:rwin
# /etc/X11/xorg.conf.d/00-keyboard.conf
	# Written by systemd-localed(8), read by systemd-localed and Xorg. It's
	# probably wise not to edit this file manually. Use localectl(1) to
	# instruct systemd-localed to update it.
	Section "InputClass"
			Identifier "system-keyboard"
			MatchIsKeyboard "on"
			Option "XkbLayout" "de"
			Option "XkbModel" "pc105"
			Option "XkbVariant" "nodeadkeys"
			Option "XkbOptions" "compose:rwin"
	EndSection


# you may check a startx

# Themes and Icons
# Folder: ~/.themes ~/.icons
# copy the tar.gz and make tar xvzf ...
# Configuration with tool lxappearance


# ZSH with
# Oh my ZSH:
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
# i have my own .zshrc - grab it
# and get the plugins we want to use:
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
yay autojump
# don't forget your personal .zshrc

