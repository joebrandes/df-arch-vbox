All you - or i - need for a fresh Arch Linux Virtual Box install

Reminder: this is a Xmonad pure Xorg Environment and needs all the stuff installed
i tell you about in other READMEs or Projects ;-)

In short

    # get me some SW for Arch Linux with pacman:

    # maybe some stuff already installed (see vim , nano, neofetch)
    sudo pacman -S xf86-video-fbdev xorg xorg-xinit xterm lxappearance \
        nitrogen picom awesome-terminal-fonts xdotool gsimplecal \
        xmonad xmonad-contrib xmobar dmenu dzen2 rofi trayer volumeicon \
        rxvt-unicode urxvt-perls firefox mc neovim neofetch xed \
        vim nano ranger pcmanfm w3m imlib2 highlight poppler zsh \
        cowsay lolcat sl cmatrix fortune-mod figlet asciiquarium banner \
        nemo nemo-fileroller nemo-image-converter nemo-preview nemo-terminal

    # and maybe you like also:
    sudo pacman -S alacritty qutebrowser pulseaudio pulseaudio-alsa alsa-utils
