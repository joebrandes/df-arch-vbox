Distrotube: https://distrotube.com/ (Youtube: https://www.youtube.com/watch?v=PQgyW10xD8s)
LearnLinuxTV: https://www.learnlinux.tv/ (Youttube: (https://www.youtube.com/watch?v=Lq4cbp5AOZM)
Arch Wiki for Hyper-V: https://wiki.archlinux.org/index.php/Hyper-V
=======
Basis-/Test-Installation zuletzt getestet Ende 2020
=======

Installation auf Hyper-V (127 GB VHDX)

Von ISO booten und los geht es...
(Vbox-/Hyper-V-Reminder: really hypervisorlaunchtype Auto ??)

Deutsche Tastatur: (Tipp: - liegt auf ß bei englischem Layout)
============
loadkeys de-latin1

Tipps for Seminar: (very big font - type setfont to set back to normal)
setfont latarcyrheb-sun32.psfu.gz

Pacman Archive aktualisieren:
=============================
pacman -Syyy
Ggf. verbessern mit /etc/pacman.d/mirrorlist 

Partitions:
======
1) EFI +500M
2) Boot +500M Linux
3) "Rest" als Linux LVM
======
Using fdisk: (alternatively with gdisk or cfdisk)
fdisk -l   (lists out the partitions)
fdisk /dev/sda
In fdisk, "m" for help
In fdisk, "o" for DOS partition or "g" for GPT
In fdisk, "n" for add new partition
In fdisk, "p" for primary partition (if using MBR instead of GPT)
In fdisk, "t" to change partition type
In fdisk, "w" (write table to disk)

Using gdisk: (Standardtool für GPT)
============
o - new empty GUID Part. Table (GPT)
t - types: ef00 (EFI), 8300 (Linux filesystem), 8e00 (Linux LVM)
w - write

Make filesystem: (Swap later as File)
==========
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2

================================================================================= 
(nur für eigenen *wichtige* VMs oder Darstellung crypt/luks)
Setup Encryption:
=================
cryptsetup luksFormat /dev/sda3
HINT: Passphrase !!IMPORTANT!!

cryptsetup open --type luks /dev/sda3 lvm     (lvm a name for pv our setup)
=================================================================================

LVM Handling:
=============
pvcreate /dev/sda3 
(crypt version: pvcreate /dev/mapper/lvm )

vgcreate volgroup0 /dev/sda3
(crpyt version: vgcreate volgroup0 /dev/mapper/lvm )

lvcreate -L 40GB volgroup0 -n lv_root
lvcreate -L 50GB volgroup0 -n lv_home       (we leave appr. 30% alone for LVM Snapshots)
alternatively: lvcreate -l 100%FREE volgroup0 -n lv_home (for the rest of Disk)

mkfs.ext4 /dev/volgroup0/lv_root
mkfs.ext4 /dev/volgroup0/lv_home


Base Install/Mounts: (another Vbox-Reminder: really hypervisorlaunchtype off ??)
====================
mount /dev/volgroup0/lv_root /mnt          (mounts it to mnt on live image)
mkdir /mnt/home 
mount /dev/volgroup0/lv_home /mnt/home
mkdir /mnt/boot
mount /dev/sda2 /mnt/boot

mkdir /mnt/etc
genfstab -U -p /mnt >> /mnt/etc/fstab (die EFI-Partition wird nicht berücksichtigt!)
checken mit cat /mnt/etc/fstab

Base Packages: (here also with alternative Linux Kernel linux-lts)
==============
pacstrap -i /mnt base linux linux-firmware ... linux-lts

Chroot:
=====
arch-chroot /mnt (change into root directory of our new installation)

Installations/Completions (also Tests for arch-chroot Env.)
==================
pacman -S nano vim linux-headers linux-lts-headers
pacman -S base-devel dialog      (openssh networkmanager ... network-manager-applet    ... see below)

Preparations LVM - Grub
=======================
pacman -S lvm2
nano /etc/mkinitcpio.conf
Change: HOOKS=" ....  block encrypt lvm2 filesystem ....)
mkinitcpio -p linux
mkinitcpio -p linux-lts      # if you installed linux-lts

# locales - de_DE.UTF8 UTF-8
nano /etc/locale.gen
locale-gen

# LANG auskommentieren in /etc/locale.conf oder
echo LANG=de_DE.UTF-8 > /etc/locale.conf

# no dead keys - dann kann man Akzentzeichen aus 2 Zeichen zusammensetzen
echo KEYMAP=de-latin1-nodeadkeys > /etc/vconsole.conf

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# setting the hardware clock
hwclock --systohc


# hostname
nano /etc/hostname
Content /etc/hostname:
----------------------------
archvbox

nano /etc/hosts
Content /etc/hosts:
------------------------
127.0.0.1  localhost
::1             localhost
127.0.1.1 archvbox.localdomain archvbox

Users and passwords:
=============
passwd (set root pass)
useradd -m username (make another user)
passwd username (set that user's password)
usermod -aG wheel,audio,video,optical,storage username

Sudo:
====

pacman -S sudo
(ggf. EDITOR=nano visudo)
visudo  (Aufruf für das Bearbeiten von /etc/sudoers)
auskommentieren: %wheel ALL=(ALL) ALL

GRUB:
===
pacman -S grub
pacman -S efibootmgr dosfstools os-prober mtools

Default Grub: (das meiste für crypt Volume!)
=============
nano /etc/default/grub
uncomment: GRUB_ENABLE_CRYPTODISK=y
prepare: GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3   cryptdevice=/dev/sda3:volgroup0:allow-discards    quiet"

Auflösung optimieren: ... quiet video=hyperv_fb:1920x1080 ...


EFI: (alternative - MBR: grub-install && grub-mkconfig -o /boot/grub/grub.cfg)
====
mkdir /boot/EFI (if doing UEFI)
mount /dev/sda1 /boot/EFI  #Mount FAT32 EFI partition (if doing UEFI)
grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --recheck       (if doing UEFI)

(check ls -al /boot/grub/locale)

grub-mkconfig -o /boot/grub/grub.cfg

Swapfile:
=========
fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile 
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

You may install on **real machines** or Vbox (kein Installationen bei Hyper-V)
============================================
pacman -S intel-ucode  (or amd-ucode)
pacman -S mesa
pacman -S nvidia nvidia-utils   (or/and nvidia-lts)
pacman -S virtualbox-guest-utils xf64-video-vmware (or xf86-video-fbdev)

Networking:
=======
pacman -S networkmanager       (later: network-manager-applet)
systemctl enable NetworkManager

ssh: (openSSH Server)
====
pacman -S openssh
(systemctl start sshd)
systemctl enable sshd

=====
Reboot:
=====
exit the chroot by typing "exit"
umount /mnt (unmounts /mnt; if busy: umount -l /mnt)
reboot (or shutdown now if doing this in VirtualbBox)
Remember to detach the ISO in VirtualBox before reboot.


Post-Installation:

System aktualisieren:
=============
pacman -Syu


=========================================================================================================================
Hyper-V: just for Hyper-V **integration services** 
see: https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/manage/manage-hyper-v-integration-services )
(for X-Support later: xf86-video-fbdev)
=========================================================================================================================
pacman -S hyperv        # do not install - only for those 'integrations*
systemctl enable hy_fcopy_daemon.service
systemctl enable hy_kvp_daemon.service
systemctl enable hy_vss_daemon.service
=========================================================================================================================

Mounts checken: (siehe /etc/fstab)
===============
    [joeb@archvbox ~]$ cat /etc/fstab
    # Static information about the filesystems.
    # See fstab(5) for details.

    # <file system> <dir> <type> <options> <dump> <pass>
    # /dev/sda3
    UUID=024d41e5-6f65-4ada-a8ad-44f5fc89b575	/         	ext4      	rw,relatime	0 1

    # /dev/sda2
    UUID=9d5b844b-22e3-4d6c-b46d-0b14c8fe61ca	none      	swap      	defaults  	0 0

    /swapfile none swap sw 0 0

Man-Pages: 
=======
pacman -S man-db man-pages      (ggf. man-pages-de)

yay - AUR Helper
==========
pacman -S git base-devel     (Standardtools bereitstellen)
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si


=============================================================
INFO: till here everything working Arch as Hyper-V Machines !
=============================================================
Hint: Snapshot and then tests with X-server / Graphic-Env   !
=============================================================


