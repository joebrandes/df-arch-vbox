#!/bin/sh
# full sw install after Base install Arch Linux on Hyper-V
# after Base install with x-server sw and test with TWM

# Step 2
pacman -S lxappearance \
	nitrogen picom awesome-terminal-fonts xdotool xclip gsimplecal arandr \
	xmonad xmonad-contrib xmobar dmenu dzen2 rofi trayer volumeicon \
	rxvt-unicode urxvt-perls firefox mc neovim neofetch xed \
	vim nano ranger pcmanfm w3m imlib2 highlight poppler sxiv zsh \
	cowsay lolcat sl cmatrix fortune-mod figlet asciiquarium banner \
	nemo nemo-fileroller nemo-image-converter nemo-preview nemo-terminal \
    alacritty qutebrowser pulseaudio pulseaudio-alsa alsa-utils
