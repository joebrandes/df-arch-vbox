# ausführlichere Darstellung bei der VirtualBox install

# Minimalinstallation video/x-Server
sudo pacman -S xf86-video-fbdev xorg xorg-xinit xterm
# (everything xorg, x-Libs, mesa, ...)

# prepare startx
cp /etc/X11/xinit/xinitrc ~/.xinitrc
# Test X with Standard-TWM (see last 5 lines in .xinitrc)

# prepare FullHD 1920x1080 - if not previously configured
# /etc/default/grub:
# "... quiet video=hyperv_fb:1920x1080"
grub-mkconfig -o /boot/grub/grub.cfg

# install the rest and prepare xmonad env with xmonad --recompile

# german keymap:
sudo localectl set-x11-keymap de pc105 nodeadkeys compose:rwin

# test with picom-jonaburg-git: OK!